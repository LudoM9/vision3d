import cv2
import numpy as np
from matplotlib import pyplot as plt

img1 = cv2.imread('TP2_1/Paires_Stereo/Gauche/gauche_2.png',0)  #queryimage # left image
img2 = cv2.imread('TP2_1/Paires_Stereo/Droite/droite_2.png',0) #trainimage # right image
sift = cv2.SIFT_create()

# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

# FLANN parameters
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)
flann = cv2.FlannBasedMatcher(index_params,search_params)
matches = flann.knnMatch(des1,des2,k=2)

good = []
pts1 = []
pts2 = []

# ratio test as per Lowe's paper
for i,(m,n) in enumerate(matches):
    if m.distance < 0.8*n.distance:
        good.append(m)
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)

pts1 = np.int32(pts1)
pts2 = np.int32(pts2)
F, mask = cv2.findFundamentalMat(pts1,pts2,cv2.FM_LMEDS)

# We select only inlier points
pts1 = pts1[mask.ravel()==1]
pts2 = pts2[mask.ravel()==1]

def drawlines(img1,img2,lines,pts1,pts2):
    ''' img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines '''
    r,c = img1.shape
    img1 = cv2.cvtColor(img1,cv2.COLOR_GRAY2BGR)
    img2 = cv2.cvtColor(img2,cv2.COLOR_GRAY2BGR)
    for r,pt1,pt2 in zip(lines,pts1,pts2):
        color = tuple(np.random.randint(0,255,3).tolist())
        x0,y0 = map(int, [0, -r[2]/r[1] ])
        x1,y1 = map(int, [c, -(r[2]+r[0]*c)/r[1] ])
        img1 = cv2.line(img1, (x0,y0), (x1,y1), color,1)
        img1 = cv2.circle(img1,tuple(pt1),5,color,-1)
        img2 = cv2.circle(img2,tuple(pt2),5,color,-1)
    return img1,img2

# Find epilines corresponding to points in right image (second image) and
# drawing its lines on left image
lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1,1,2), 2,F)
lines1 = lines1.reshape(-1,3)
img5,img6 = drawlines(img1,img2,lines1,pts1,pts2)

# Find epilines corresponding to points in left image (first image) and
# drawing its lines on right image
lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1,1,2), 1,F)
lines2 = lines2.reshape(-1,3)
img3,img4 = drawlines(img2,img1,lines2,pts2,pts1)

plt.subplot(121),plt.imshow(img5)
plt.subplot(122),plt.imshow(img3)
plt.show()

# Les points de l'image de gauche et ceux de l'image de droite sont bien alignés sur les
#       lignes épipolaires.
# Il y a une bonne correspondance entre les points des deux images.

# Matching avec BFMatcher
# BFMatcher with default params
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=2)
# Apply ratio test
good = []
pts1 = []
pts2 = []
for m,n in matches:
    if m.distance < 0.5*n.distance:
        good.append([m])
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)

# cv2.drawMatchesKnn expects list of lists as matches.
img7 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=2)
plt.imshow(img7)
plt.show()

# BFMatcher refers to a Brute-force matcher that is nothing, but a distance computation used to
#       match the descriptor of one feature from the first set with each of the other features
#       in the second set. The nearest is then returned.

# Les paires appariées sont en grande majorité cohérentes avec les lignes épipolaires. Seuls
#       quelques points sont mal appariés.
# Si on réduit le paramètre de distance de 0.8 à 0.5, on a moins de paires appariées mais
#      elles sont plus cohérentes avec les lignes épipolaires et toutes les paires sont
#      correctes.

# Fundamental Matrix
# Fundamental Matrix is a 3x3 matrix which relates corresponding points in stereo images.
#       It is denoted by F.

pts1 = np.float32(pts1)
pts2 = np.float32(pts2)
F = cv2.findFundamentalMat(pts1, pts2, cv2.FM_RANSAC, 5, 0.9, 100)
print("F = ", F[0])

# Check if F is a singular matrix
def issingular(F):
    return np.linalg.det(F) < 1e-7

print("F is singular: ", issingular(F[0]))

# Random sample consensus (RANSAC) is an iterative method to estimate parameters of a mathematical
#       model from a set of observed data that contains outliers, when outliers are to be accorded
#       no influence on the values of the estimates.

# Les droites épipolaires représentent les lignes sur lesquelles se trouvent les points. Elles
#      sont déterminées par la matrice fondamentale F en multipliant les coordonnées des points
#      par la matrice fondamentale F.

# Cartes des profondeurs
imgL = cv2.imread('TP2_1/Paires_Stereo/Gauche/tsukuba_l.jpg',0)
imgR = cv2.imread('TP2_1/Paires_Stereo/Droite/tsukuba_r.jpg',0)

stereo = cv2.StereoBM_create(numDisparities=96, blockSize=21)
disparity = stereo.compute(imgL,imgR)
plt.subplot(131),plt.imshow(imgL)
plt.subplot(132),plt.imshow(imgR)
plt.subplot(133),plt.imshow(disparity,'gray')
plt.show()
