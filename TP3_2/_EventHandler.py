# Author : Mirado Rajaomarosata
# Date : 22/09/2023

import glob
import numpy as np
import pygame
from pygame.locals import *
from OpenGL.GL import *
import cv2 as cv
import time


class EventHandler():

    def __init__(self, display):
        self.mouse_down = False
        self.old_mouse_pos = (0, 0)
        self.t = np.zeros((3, 1))
        self.R = np.eye(3)
        self.n = 0

    def handle_events(self):
        speed = 0.2
        translation_speed = speed
        rotation_speed = speed * 0.2

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    self.mouse_down = True
                    self.old_mouse_pos = event.pos
                elif event.button == 4: # Scroll up
                    self.t += translation_speed * self.R @ np.array([[0], [0], [1]])
                elif event.button == 5: # Scroll down
                    self.t -= translation_speed * self.R @ np.array([[0], [0], [1]])
            elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                self.mouse_down = False
            elif event.type == pygame.MOUSEMOTION and self.mouse_down: # Clic gauche + mouvement souris
                dx, dy = event.pos[0] - self.old_mouse_pos[0], event.pos[1] - self.old_mouse_pos[1]
                self.old_mouse_pos = event.pos
                if dx != 0:
                    self.R = np.array([[np.cos(rotation_speed * dx), 0, np.sin(rotation_speed * dx)],
                                       [0, 1, 0],
                                       [-np.sin(rotation_speed * dx), 0, np.cos(rotation_speed * dx)]]) @ self.R
                if dy != 0:
                    self.R = np.array([[1, 0, 0],
                                       [0, np.cos(rotation_speed * dy), -np.sin(rotation_speed * dy)],
                                       [0, np.sin(rotation_speed * dy), np.cos(rotation_speed * dy)]]) @ self.R

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.t += translation_speed * np.array([[1], [0], [0]])
        if keys[pygame.K_RIGHT]:
            self.t -= translation_speed * np.array([[1], [0], [0]])
        if keys[pygame.K_UP]:
            self.t -= translation_speed * np.array([[0], [1], [0]])
        if keys[pygame.K_DOWN]:
            self.t += translation_speed * np.array([[0], [1], [0]])

        if keys[pygame.K_p]:

            buffer = glReadPixels(0, 0, 640, 640, GL_RGBA, GL_UNSIGNED_BYTE) # Read the left image
            screen_surf = pygame.image.fromstring(buffer, [640, 640], "RGBA")
            pygame.image.save(screen_surf, f"stereo_calibration_image/screenshot_left_{self.n}.png")

            buffer = glReadPixels(640, 0, 640, 640, GL_RGBA, GL_UNSIGNED_BYTE) # Read the right image
            screen_surf = pygame.image.fromstring(buffer, [640, 640], "RGBA")
            pygame.image.save(screen_surf, f"stereo_calibration_image/screenshot_right_{self.n}.png")

            self.n += 1 # Increment the number of images saved
            time.sleep(2.0) # Wait for the image to be saved
