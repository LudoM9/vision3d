# Author : Mirado Rajaomarosata
# Date : 22/09/2023


import pygame
from pygame.locals import *
from OpenGL.GL import *

from _OBJFileLoader import OBJ
from _StereoCamera import *


# Initialiser Pygame et créer une fenêtre
pygame.init()
WIDTH, HEIGHT = 600 * 3, 600
display = (WIDTH, HEIGHT)
pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

# Chargement du fichier CAO obj
obj = OBJ("cv_checkerboard.obj")
obj = OBJ("solids.obj")

# Couleur du fond
glClearColor(1.0, 0.8, 0.8, 1.0)
# glClearColor(1.0, 1., 1., 1.0)

# Source de lumière, ombre et profondeur
glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)
obj.generate()

camera_stereo = StereoCamera((600, 600), np.array([[-1.], [1.], [0.]]))

STATE = 0 # Q1 à Q5 (Il suffit de changer l'objet à la ligne 20/21)
# Q6 dans le fichier camera_calibration.py
STATE = 1 # Q7 à Q10
STATE = 2 # Q11 à Q15

if STATE == 0:
    while True:
        camera_stereo.handle_events()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        camera_stereo.perspective_projection(obj)

        pygame.display.flip()
        pygame.time.wait(10)
elif STATE == 1:
    while True:
        camera_stereo.handle_events()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        camera_stereo.perspective_projection(obj)

        camera_stereo.rectification()

        pygame.display.flip()
        pygame.time.wait(10)
elif STATE == 2:
    while True:
        camera_stereo.handle_events()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        camera_stereo.perspective_projection(obj)

        camera_stereo.sparse_stereo()

        pygame.display.flip()
        pygame.time.wait(10)
