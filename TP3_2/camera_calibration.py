import cv2
import numpy as np

from _Camera import *

def calibrate_camera(images_folder, left):
    images_names = sorted(glob.glob(images_folder))
    if left:
        images_names = images_names[:len(images_names) // 2]
    else:
        images_names = images_names[len(images_names) // 2:]

    images = []
    for imname in images_names:
        im = cv.imread(imname, 1)
        images.append(im)

    # criteria used by checkerboard pattern detector.
    # Change this if the code can't find the checkerboard
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    rows = 6  # number of checkerboard rows.
    columns = 9  # number of checkerboard columns.
    world_scaling = 1.  # change this to the real world square size.

    # coordinates of squares in the checkerboard world space
    objp = np.zeros((rows * columns, 3), np.float32)
    objp[:, :2] = np.mgrid[0:rows, 0:columns].T.reshape(-1, 2)
    objp = world_scaling * objp

    # frame dimensions. Frames should be the same size.
    width = images[0].shape[1]
    height = images[0].shape[0]

    # Pixel coordinates of checkerboards
    imgpoints = []  # 2d points in image plane.

    # coordinates of the checkerboard in checkerboard world space.
    objpoints = []  # 3d point in real world space

    for frame in images:
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

        # find the checkerboard
        ret, corners = cv.findChessboardCorners(gray, (rows, columns), None)

        if ret == True:
            # Convolution size used to improve corner detection. Don't make this too large.
            conv_size = (11, 11)

            # opencv can attempt to improve the checkerboard coordinates
            corners = cv.cornerSubPix(gray, corners, conv_size, (-1, -1), criteria)
            cv.drawChessboardCorners(frame, (rows, columns), corners, ret)
            cv.imshow('img', frame)
            k = cv.waitKey(500)

            objpoints.append(objp)
            imgpoints.append(corners)

    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, (width, height), None, None)

    return mtx, dist


def stereo_calibrate(mtx1, dist1, mtx2, dist2, frames_folder):
    #   read the synched frames
    images_names = glob.glob(frames_folder)
    images_names = sorted(images_names)
    c1_images_names = images_names[:len(images_names) // 2]
    c2_images_names = images_names[len(images_names) // 2:]

    c1_images = []
    c2_images = []
    for im1, im2 in zip(c1_images_names, c2_images_names):
        _im = cv.imread(im1, 1)
        c1_images.append(_im)

        _im = cv.imread(im2, 1)
        c2_images.append(_im)

    # change this if stereo calibration not good.
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 100, 0.0001)

    rows = 6  # number of checkerboard rows.
    columns = 9  # number of checkerboard columns.
    world_scaling = 1.  # change this to the real world square size. Or not.

    # coordinates of squares in the checkerboard world space
    objp = np.zeros((rows * columns, 3), np.float32)
    objp[:, :2] = np.mgrid[0:rows, 0:columns].T.reshape(-1, 2)
    objp = world_scaling * objp

    # frame dimensions. Frames should be the same size.
    width = c1_images[0].shape[1]
    height = c1_images[0].shape[0]

    # Pixel coordinates of checkerboards
    imgpoints_left = []  # 2d points in image plane.
    imgpoints_right = []

    # coordinates of the checkerboard in checkerboard world space.
    objpoints = []  # 3d point in real world space

    for frame1, frame2 in zip(c1_images, c2_images):
        gray1 = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
        gray2 = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
        c_ret1, corners1 = cv.findChessboardCorners(gray1, (6, 9), None)
        c_ret2, corners2 = cv.findChessboardCorners(gray2, (6, 9), None)

        if c_ret1 == True and c_ret2 == True:
            corners1 = cv.cornerSubPix(gray1, corners1, (11, 11), (-1, -1), criteria)
            corners2 = cv.cornerSubPix(gray2, corners2, (11, 11), (-1, -1), criteria)

            cv.drawChessboardCorners(frame1, (6, 9), corners1, c_ret1)
            cv.imshow('img', frame1)

            cv.drawChessboardCorners(frame2, (6, 9), corners2, c_ret2)
            cv.imshow('img2', frame2)
            cv.waitKey(500)

            objpoints.append(objp)
            imgpoints_left.append(corners1)
            imgpoints_right.append(corners2)

    stereocalibration_flags = cv.CALIB_FIX_INTRINSIC
    ret, CM1, dist1, CM2, dist2, R, T, E, F = cv.stereoCalibrate(objpoints, imgpoints_left, imgpoints_right, mtx1,
                                                                 dist1,
                                                                 mtx2, dist2, (width, height), criteria=criteria,
                                                                 flags=stereocalibration_flags)

    return R, T, E, F

if __name__ == '__main__':
    mtx1, dist1 = calibrate_camera("stereo_calibration_image/*.png", True)
    mtx2, dist2 = calibrate_camera("stereo_calibration_image/*.png", False)
    R, t, E, F = stereo_calibrate(mtx1, dist1, mtx2, dist2, "stereo_calibration_image/*.png")
    print("R = ", R)
    print("t = ", t)
    print("E = ", E)
    print("F = ", F)
    print("mtx1 = ", mtx1)
    print("dist1 = ", dist1)
    print("mtx2 = ", mtx2)
    print("dist2 = ", dist2)
    np.savez("calibration.npz", mtx1=mtx1, dist1=dist1, mtx2=mtx2, dist2=dist2, R=R, t=t, E=E, F=F)
    
# Q6

# La calibration stéréo consiste à trouver les paramètres intrinsèques et extrinsèques des deux caméras
# qui composent le système stéréo.
# Les paramètres intrinsèques sont les paramètres qui dépendent de la caméra elle-même, comme la focale,
# la distorsion, etc.
# Les paramètres extrinsèques sont les paramètres qui dépendent de la position de la caméra par rapport
# au monde, comme la rotation et la translation.

# La calibration stéréo est plus complexe que la calibration mono car il faut trouver les paramètres
# intrinsèques et extrinsèques de deux caméras en même temps.

# Pour cela, on utilise un objet dont on connaît les dimensions réelles et on le prend en photo avec
# les deux caméras. On peut ensuite utiliser les coordonnées des points de l'objet dans les deux images
# pour trouver les paramètres intrinsèques et extrinsèques des deux caméras. On peut ensuite utiliser
# ces paramètres pour reconstruire la scène en 3D.