import cv2
import numpy as np
import pygame.transform

from _Camera import *


class StereoCamera(Camera):
    def __init__(self, display, dt=np.array([[1.], [1.], [0.]])):
        super().__init__(display)
        self.dt = dt

        self.sift = cv2.SIFT_create()
        self.bf = cv2.BFMatcher()

        WIDTH, HEIGHT = display[0] * 3, display[1]
        self.left_screen = [0, 0, WIDTH // 3, HEIGHT]
        self.right_screen = [WIDTH // 3, 0, WIDTH // 3, HEIGHT]
        self.reconstruction_screen = [2 * WIDTH // 3, 0, WIDTH // 3, HEIGHT]

    def perspective_projection(self, obj):
        glViewport(*self.left_screen)
        glMatrixMode(GL_PROJECTION)
        t_left = self.t
        glLoadMatrixf(self.perspective_matrix(self.R, t_left).T)
        glMatrixMode(GL_MODELVIEW)
        obj.render()

        glViewport(*self.right_screen)
        glMatrixMode(GL_PROJECTION)
        t_right = self.t + self.dt
        glLoadMatrixf(self.perspective_matrix(self.R, t_right).T)
        glMatrixMode(GL_MODELVIEW)
        obj.render()

    def drawlines(self, img1, img2, lines, pts1, pts2, kp=True):
        r, c = img1.shape
        img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
        img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
        for r, pt1, pt2 in zip(lines, pts1, pts2):
            color = tuple(np.random.randint(0, 255, 3).tolist())
            x0, y0 = map(int, [0, -r[2] / r[1]])
            x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])
            img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 1)
            if kp:
                img1 = cv2.circle(img1, tuple(pt1), 5, color, -1)
                img2 = cv2.circle(img2, tuple(pt2), 5, color, -1)
        return img1, img2

    def drawpoints(self, img1, img2, pts1, pts2):
        for pt1 in pts1:
            color = tuple(np.random.randint(0, 255, 3).tolist())
            img1 = cv2.circle(img1, (int(pt1[0]), int(pt1[1])), 5, color, -1)
        for pt2 in pts2:
            color = tuple(np.random.randint(0, 255, 3).tolist())
            img2 = cv2.circle(img2, (int(pt2[0]), int(pt2[1])), 5, color, -1)

    def rectification(self):
        """Utilisation de la méthode SIFT pour trouver les points d'intérêt et les associer"""

        buffer = glReadPixels(0, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "RGBA")
        img1 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)

        buffer = glReadPixels(600, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "RGBA")
        img2 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        img2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

        kp1, des1 = self.sift.detectAndCompute(img1, None)
        kp2, des2 = self.sift.detectAndCompute(img2, None)

        matches = self.bf.knnMatch(des1, des2, k=2)
        good = []
        pts1 = []
        pts2 = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good.append(m)
                pts2.append(kp2[m.trainIdx].pt)
                pts1.append(kp1[m.queryIdx].pt)

        pts1 = np.int32(pts1)
        pts2 = np.int32(pts2)

        F, mask = cv.findFundamentalMat(pts1, pts2, cv.FM_RANSAC)
        if F is None or F.shape != (3, 3):
            return
        else:
            pts1 = pts1[mask.ravel() == 1]
            pts2 = pts2[mask.ravel() == 1]

            lines1 = cv.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
            lines1 = lines1.reshape(-1, 3)
            img5, img6 = self.drawlines(img1, img2, lines1, pts1, pts2)
            lines2 = cv.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
            lines2 = lines2.reshape(-1, 3)
            img3, img4 = self.drawlines(img2, img1, lines2, pts2, pts1)
            cv.imshow('Epilines in left image', img5)
            cv.imshow('Epilines in right image', img3)
            cv.waitKey(1)

    def sparse_stereo(self):
        buffer = glReadPixels(0, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "RGBA")
        img1 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        imgleft = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)

        buffer = glReadPixels(600, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "RGBA")
        img2 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        imgright = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

        # Chargement des paramètres de calibration:
        calibration = np.load("calibration.npz")
        parameters = ["mtx1", "dist1", "mtx2", "dist2", "R", "t", "E", "F"]
        mtx1, dist1, mtx2, dist2, R, t, E, F = [calibration[i] for i in parameters]

        # Rectification des images:
        imgleft = cv.undistort(imgleft, mtx1, dist1, None, mtx1)
        imgright = cv.undistort(imgright, mtx2, dist2, None, mtx2)

        kp1, des1 = self.sift.detectAndCompute(img1, None)
        kp2, des2 = self.sift.detectAndCompute(img2, None)

        matches = self.bf.knnMatch(des1, des2, k=2)
        good = []
        pts1 = []
        pts2 = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good.append(m)
                pts2.append(kp2[m.trainIdx].pt)
                pts1.append(kp1[m.queryIdx].pt)
            
        pts1 = np.int32(pts1)
        pts2 = np.int32(pts2)
        
        F, mask = cv.findFundamentalMat(pts1, pts2, cv.FM_RANSAC)
        if F is None or F.shape != (3, 3):
            return
        else:
            pts1 = pts1[mask.ravel() == 1]
            pts2 = pts2[mask.ravel() == 1]

            lines1 = cv.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
            lines1 = lines1.reshape(-1, 3)
            img5, img6 = self.drawlines(imgleft, imgright, lines1, pts1, pts2)
            lines2 = cv.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
            lines2 = lines2.reshape(-1, 3)
            img3, img4 = self.drawlines(imgright, imgleft, lines2, pts2, pts1)
            
            distances_left = (np.abs(lines1[:, 0] * pts1[:, 0] + lines1[:, 1] * pts1[:, 1] + lines1[:, 2])
                              / np.sqrt(lines1[:, 0] * lines1[:, 0] + lines1[:, 1] * lines1[:, 1]))
            distances_right = (np.abs(lines2[:, 0] * pts2[:, 0] + lines2[:, 1] * pts2[:, 1] + lines2[:, 2])
                               / np.sqrt(lines2[:, 0] * lines2[:, 0] + lines2[:, 1] * lines2[:, 1]))

            distances_left = distances_left < 10
            distances_right = distances_right < 10
            pts1 = pts1[distances_left]
            pts2 = pts2[distances_right]
            self.drawpoints(imgleft, imgright, pts1, pts2)
            img3 = np.hstack((imgright, imgleft))
            cv2.imshow("Match", img3)
            cv2.waitKey(1)

    # Résumé de la méthode sparse_stereo:
    # 1. Charger les images de gauche et de droite
    # 2. Charger les paramètres de calibration
    # 3. Rectifier les images
    # 4. Trouver les points d'intérêt et les associer
    # 5. Trouver la matrice fondamentale
    # 6. Calculer les lignes épipolaires
    # 7. Calculer les distances entre les points et les lignes épipolaires
    # 8. Afficher les points restants sur les images
