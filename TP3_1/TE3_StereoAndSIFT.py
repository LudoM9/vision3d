import cv2
import numpy as np
from matplotlib import pyplot as plt


img1 = cv2.imread('tsukuba_l.jpg',0) # left image
img2 = cv2.imread('tsukuba_r.jpg',0) # right image

sift = cv2.SIFT_create()
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

img1_kp = cv2.drawKeypoints(img1, kp1, None, color=(0,255,0), flags=0)
plt.imshow(img1_kp), plt.title('Detected keypoints in left image'), plt.show()
img2_kp = cv2.drawKeypoints(img2, kp2, None, color=(0,255,0), flags=0)
plt.figure()
plt.imshow(img2_kp), plt.title('Detected keypoints in right image'), plt.show()

bf = cv2.BFMatcher() 
matches = bf.knnMatch(des1,des2,k=2)
good = []
for m,n in matches:
    if m.distance < 0.75*n.distance:
        good.append([m])

img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
plt.figure()
plt.imshow(img3),plt.title('avec BF Matcher, methode match()'), plt.show()


