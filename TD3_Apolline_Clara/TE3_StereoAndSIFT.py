import cv2
import numpy as np
from matplotlib import pyplot as plt

img1 = cv2.imread('tsukuba_l.jpg',0) # left image
img2 = cv2.imread('tsukuba_r.jpg',0) # right image

sift = cv2.SIFT_create()
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

img1_kp = cv2.drawKeypoints(img1, kp1, None, color=(0,255,0), flags=0)
plt.imshow(img1_kp), plt.title('Detected keypoints in left image'), plt.show()
img2_kp = cv2.drawKeypoints(img2, kp2, None, color=(0,255,0), flags=0)
plt.figure()
plt.imshow(img2_kp), plt.title('Detected keypoints in right image'), plt.show()

bf = cv2.BFMatcher() 
matches = bf.knnMatch(des1,des2,k=2)
good = []
for m,n in matches:
    if m.distance < 0.75*n.distance:
        good.append([m])

img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
plt.figure()
plt.imshow(img3),plt.title('avec BF Matcher, methode match()'), plt.show()

list_couple = [('gauche_2.png', 'droite_2.png'),
               ('tsukuba_l.jpg', 'tsukuba_r.jpg'),
               ('view1.png', 'view5.png')]


def drawlines(img1, img2, lines, pts1, pts2):
    """ img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines """
    r, c = img1.shape
    img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
    img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        x0, y0 = map(int, [0, -r[2] / r[1]])
        x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])
        img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 1)
        img1 = cv2.circle(img1, tuple(pt1), 5, color, -1)
        img2 = cv2.circle(img2, tuple(pt2), 5, color, -1)
    return img1, img2


def epipolar_geometry():
    """Q.2. Détection des points d'intérêts"""
    sift_matcher = cv2.SIFT_create()
    bf_matcher = cv2.BFMatcher()
    sift_matcher = cv2.SIFT_create(edgeThreshold=5, contrastThreshold=0.05)
    """Discussion des paramètres :
        edgeThreshold : seuil de détection de contours, plus il est grand plus on détect de points
        contrastThreshold : seuil de contraste, plus il est grand moins on détect de points
    """
    bf_matcher = cv2.BFMatcher(normType=cv2.NORM_L1, crossCheck=True)
    """Discussion des paramètres :
        normType : type de norme utilisée pour calculer la distance entre descripteurs
        crossCheck : si True, on ne garde que les matchs qui sont réciproques
    """
    for couple in list_couple[:1]:
        img_l = cv2.imread(couple[0], 0)
        img_r = cv2.imread(couple[1], 0)
        cv2.imshow(couple[0], img_l)
        cv2.imshow(couple[1], img_r)
        cv2.waitKey(0)

        kp_l, des_l = sift_matcher.detectAndCompute(img_l, None)
        kp_r, des_r = sift_matcher.detectAndCompute(img_r, None)
        cv2.imshow(couple[0], cv2.drawKeypoints(img_l, kp_l, img_l, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS))
        cv2.imshow(couple[1], cv2.drawKeypoints(img_r, kp_r, img_r, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS))
        cv2.waitKey(0)
        """Il semble que le détecteur de points d'intérêt SIFT ressort des points
        et descripteurs similaires pour les 2 images des 2 dernières paires,
        mais ceux des 2 images de la première paire semble assez différents."""

        matches = bf_matcher.match(des_l, des_r)
        matches = sorted(matches, key=lambda x: x.distance)
        print("Nombre de points de paires trouvées pour : " + couple[0], len(matches))
        img = cv2.drawMatches(img_l, kp_l, img_r, kp_r, matches[:50], None, flags=2)
        cv2.imshow("matches", img)
        cv2.waitKey(0)
        """On voit que le brute force matcher fontionne très bien pour 
        les deux dernières paires d'images, mais pas pour la première, où on a 
        beaucoup de mauvais matchs dans les 50 premiers.
        """

        """Q.3. Calcul de la matrice fondamentale : plusieurs méthode possible.
        La matrice fondamentale (F) est une matrice 3x3 qui permet de calculer la trajectoire des droites épipolaires.
        Elle est sensée relier les paires de points de l'image de gauche à ceux de l'image de droite.
        Avec la collection de paires estimées précédemment, on peut tenter d'estimer F en utilisant 
        la méthode des moindres carrés ou la méthode RANSAC.
        
        La méthode RANSAC est une méthode itérative, qui permet d'éliminer les mauvaises paires ici. 
        C'est une méthode qui part d'un ensemble de points, et qui à chaque itération calcule un modèle à partir 
        d'un sous-ensemble de points, puis calcule l'erreur de ce modèle sur l'ensemble des points. On garde le modèle 
        qui minimise l'erreur, et on recommence jusqu'à ce que l'erreur soit inférieure à un seuil donné.
        """
        pts1 = np.float32([kp_l[m.queryIdx].pt for m in matches[:50]])
        pts2 = np.float32([kp_r[m.trainIdx].pt for m in matches[:50]])
        F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_RANSAC)
        print("Matrice fondamentale : ", F)
        print("F est-elle singulière ? ", np.linalg.det(F) < 1e-10, ", det(F) = ", np.linalg.det(F))

        """Q.4. Les droites épipolaires.
        Ces droites représentent la projection des points de l'image d'une caméra sur l'image de la seconde. 
        La droite épipolaire correspondant à un point se calcule en le multipliant par la matrice fondamentale.
        En les dessinants ci-dessous, on a un résultat assez satisfaisant. Comme les caméras sont proches,
        les droites épipolaires sont presque parallèles.
        """
        # We select only inlier points
        pts1 = pts1[mask.ravel() == 1].astype(int)
        pts2 = pts2[mask.ravel() == 1].astype(int)
        # Find epilines corresponding to points in right image (second image) and
        # drawing its lines on left image
        lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
        lines1 = lines1.reshape(-1, 3)
        img_l, img_r = drawlines(img_l, img_r, lines1, pts1, pts2)
        cv2.imshow(couple[0], img_l)
        cv2.imshow(couple[1], img_r)
        cv2.waitKey(0)
        # Find epilines corresponding to points in left image (first image) and
        # drawing its lines on right image
        lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
        lines2 = lines2.reshape(-1, 3)
        img_l, img_r = cv2.cvtColor(img_l, cv2.COLOR_BGR2GRAY), cv2.cvtColor(img_r, cv2.COLOR_BGR2GRAY)
        img_r, img_l = drawlines(img_r, img_l, lines2, pts2, pts1)
        cv2.imshow(couple[0], img_l)
        cv2.imshow(couple[1], img_r)
        cv2.waitKey(0)


def depth_map():
    """Q.5. Calcul de la carte de disparité.
    On voit bien l'influence de chaque paramètre grâce à la fonction suivante, et on remarque
    que les résultats sont assez bons pour les 2 dernières paires d'images, mais pas pour la première.
    """
    stereo = cv2.StereoBM().create()
    # Creating a window with a slider to adjust the parameters
    cv2.namedWindow('disparity')
    cv2.createTrackbar('minDisparity', 'disparity', 0, 8, lambda x: stereo.setMinDisparity(x * 16))
    cv2.createTrackbar('numDisparities', 'disparity', 16, 8, lambda x: stereo.setNumDisparities(x * 16))
    cv2.createTrackbar('blockSize', 'disparity', 3, 50, lambda x: stereo.setBlockSize(x * 2 + 5))
    cv2.createTrackbar('textureThreshold', 'disparity', 10, 100, lambda x: stereo.setTextureThreshold(x))
    cv2.createTrackbar('uniquenessRatio', 'disparity', 15, 100, lambda x: stereo.setUniquenessRatio(x))

    couple = list_couple[2]
    img_l = cv2.imread(couple[0], 0)
    img_r = cv2.imread(couple[1], 0)
    while True:
        disparity = stereo.compute(img_l, img_r).astype(np.float32)
        min_disp = stereo.getMinDisparity()
        num_disp = stereo.getNumDisparities()
        cv2.imshow('disparity', (disparity / 16. - min_disp) / num_disp)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


if __name__ == '__main__':
    epipolar_geometry()
    depth_map()
