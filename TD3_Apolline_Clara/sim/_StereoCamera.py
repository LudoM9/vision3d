import cv2
import numpy as np
import pygame.transform

from _Camera import *


def calibrate_camera(images_folder, left):
    images_names = sorted(glob.glob(images_folder))
    if left:
        images_names = images_names[:len(images_names) // 2]
    else:
        images_names = images_names[len(images_names) // 2:]

    images = []
    for imname in images_names:
        im = cv.imread(imname, 1)
        images.append(im)

    # criteria used by checkerboard pattern detector.
    # Change this if the code can't find the checkerboard
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    rows = 6  # number of checkerboard rows.
    columns = 9  # number of checkerboard columns.
    world_scaling = 1.  # change this to the real world square size. Or not.

    # coordinates of squares in the checkerboard world space
    objp = np.zeros((rows * columns, 3), np.float32)
    objp[:, :2] = np.mgrid[0:rows, 0:columns].T.reshape(-1, 2)
    objp = world_scaling * objp

    # frame dimensions. Frames should be the same size.
    width = images[0].shape[1]
    height = images[0].shape[0]

    # Pixel coordinates of checkerboards
    imgpoints = []  # 2d points in image plane.

    # coordinates of the checkerboard in checkerboard world space.
    objpoints = []  # 3d point in real world space

    for frame in images:
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

        # find the checkerboard
        ret, corners = cv.findChessboardCorners(gray, (rows, columns), None)

        if ret == True:
            # Convolution size used to improve corner detection. Don't make this too large.
            conv_size = (11, 11)

            # opencv can attempt to improve the checkerboard coordinates
            corners = cv.cornerSubPix(gray, corners, conv_size, (-1, -1), criteria)
            cv.drawChessboardCorners(frame, (rows, columns), corners, ret)
            cv.imshow('img', frame)
            k = cv.waitKey(500)

            objpoints.append(objp)
            imgpoints.append(corners)

    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, (width, height), None, None)
    print('rmse:', ret)
    print('camera matrix:\n', mtx)
    print('distortion coeffs:', dist)
    print('Rs:\n', rvecs)
    print('Ts:\n', tvecs)

    return mtx, dist


def stereo_calibrate(mtx1, dist1, mtx2, dist2, frames_folder):
    #   read the synched frames
    images_names = glob.glob(frames_folder)
    images_names = sorted(images_names)
    c1_images_names = images_names[:len(images_names) // 2]
    c2_images_names = images_names[len(images_names) // 2:]

    c1_images = []
    c2_images = []
    for im1, im2 in zip(c1_images_names, c2_images_names):
        _im = cv.imread(im1, 1)
        c1_images.append(_im)

        _im = cv.imread(im2, 1)
        c2_images.append(_im)

    # change this if stereo calibration not good.
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 100, 0.0001)

    rows = 6  # number of checkerboard rows.
    columns = 9  # number of checkerboard columns.
    world_scaling = 1.  # change this to the real world square size. Or not.

    # coordinates of squares in the checkerboard world space
    objp = np.zeros((rows * columns, 3), np.float32)
    objp[:, :2] = np.mgrid[0:rows, 0:columns].T.reshape(-1, 2)
    objp = world_scaling * objp

    # frame dimensions. Frames should be the same size.
    width = c1_images[0].shape[1]
    height = c1_images[0].shape[0]

    # Pixel coordinates of checkerboards
    imgpoints_left = []  # 2d points in image plane.
    imgpoints_right = []

    # coordinates of the checkerboard in checkerboard world space.
    objpoints = []  # 3d point in real world space

    for frame1, frame2 in zip(c1_images, c2_images):
        gray1 = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
        gray2 = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
        c_ret1, corners1 = cv.findChessboardCorners(gray1, (6, 9), None)
        c_ret2, corners2 = cv.findChessboardCorners(gray2, (6, 9), None)

        if c_ret1 == True and c_ret2 == True:
            corners1 = cv.cornerSubPix(gray1, corners1, (11, 11), (-1, -1), criteria)
            corners2 = cv.cornerSubPix(gray2, corners2, (11, 11), (-1, -1), criteria)

            cv.drawChessboardCorners(frame1, (6, 9), corners1, c_ret1)
            cv.imshow('img', frame1)

            cv.drawChessboardCorners(frame2, (6, 9), corners2, c_ret2)
            cv.imshow('img2', frame2)
            cv.waitKey(500)

            objpoints.append(objp)
            imgpoints_left.append(corners1)
            imgpoints_right.append(corners2)

    stereocalibration_flags = cv.CALIB_FIX_INTRINSIC
    ret, CM1, dist1, CM2, dist2, R, T, E, F = cv.stereoCalibrate(objpoints, imgpoints_left, imgpoints_right, mtx1,
                                                                 dist1,
                                                                 mtx2, dist2, (width, height), criteria=criteria,
                                                                 flags=stereocalibration_flags)

    print(ret)
    return R, T, E, F


def drawlines(img1, img2, lines, pts1, pts2, kp=True):
    """ img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines """
    r, c = img1.shape
    img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
    img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        x0, y0 = map(int, [0, -r[2] / r[1]])
        x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])
        img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 1)
        if kp:
            img1 = cv2.circle(img1, tuple(pt1), 5, color, -1)
            img2 = cv2.circle(img2, tuple(pt2), 5, color, -1)
    return img1, img2


def drawpoints(img1, img2, pts1, pts2):
    for pt1 in pts1:
        color = tuple(np.random.randint(0, 255, 3).tolist())
        img1 = cv2.circle(img1, (int(pt1[0]), int(pt1[1])), 5, color, -1)
    for pt2 in pts2:
        color = tuple(np.random.randint(0, 255, 3).tolist())
        img2 = cv2.circle(img2, (int(pt2[0]), int(pt2[1])), 5, color, -1)


class StereoCamera(Camera):
    def __init__(self, display, dt):
        super().__init__(display)
        self.dt = dt

        WIDTH, HEIGHT = display[0] * 3, display[1]
        self.left_screen = [0, 0, int(WIDTH / 3), HEIGHT]
        self.right_screen = [int(WIDTH / 3), 0, int(WIDTH / 3), HEIGHT]
        self.disparity_screen = [int(2 * WIDTH / 3), 0, int(WIDTH / 3), HEIGHT]

        # pour déterminer la géométrie épipolaire
        self.sift_matcher = cv.SIFT_create()
        self.bf_matcher = cv.BFMatcher()

    def perspective_projection(self, obj):
        glViewport(*self.left_screen)
        glMatrixMode(GL_PROJECTION)
        t_left = self.t
        glLoadMatrixf(self.perspective_matrix(self.R, t_left).T)
        glMatrixMode(GL_MODELVIEW)
        obj.render()

        glViewport(*self.right_screen)
        glMatrixMode(GL_PROJECTION)
        t_right = self.t + self.dt
        glLoadMatrixf(self.perspective_matrix(self.R, t_right).T)
        glMatrixMode(GL_MODELVIEW)
        obj.render()

    def rectify_images(self, obj):
        buffer = glReadPixels(0, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "BGRA")
        img1 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        img1 = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)

        buffer = glReadPixels(600, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "BGRA")
        img2 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        img2 = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

        kp1, des1 = self.sift_matcher.detectAndCompute(img1, None)
        kp2, des2 = self.sift_matcher.detectAndCompute(img2, None)

        matches = self.bf_matcher.match(des1, des2)
        matches = sorted(matches, key=lambda x: x.distance)
        pts1 = np.float32([kp1[m.queryIdx].pt for m in matches[:30]])
        pts2 = np.float32([kp2[m.trainIdx].pt for m in matches[:30]])
        if pts1.shape[0] > 0 and pts2.shape[0] > 0:
            F, mask = cv.findFundamentalMat(pts1, pts2, cv.FM_RANSAC)
            if F is not None:
                if F.shape == (3, 3):
                    # We select only inlier points
                    pts1 = pts1[mask.ravel() == 1].astype(int)
                    pts2 = pts2[mask.ravel() == 1].astype(int)
                    # Find epilines corresponding to points in right image (second image) and
                    # drawing its lines on left image
                    lines1 = cv.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
                    lines1 = lines1.reshape(-1, 3)
                    img1, img2 = drawlines(img1, img2, lines1, pts1, pts2)
                    lines2 = cv.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
                    lines2 = lines2.reshape(-1, 3)
                    img1, img2 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY), cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
                    img2, img1 = drawlines(img2, img1, lines2, pts2, pts1)
                    img = np.hstack((img1, img2))
                    cv.imshow("epilines", img)
                    cv.waitKey(1)

                else:
                    print("F is incorrect")
                    print(F.shape)

    def sparse_stereo(self, obj):
        buffer = glReadPixels(0, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "RGBA")
        img1 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        self.obj_left = cv.cvtColor(img1, cv.COLOR_BGR2GRAY)

        buffer = glReadPixels(600, 0, 600, 600, GL_RGBA, GL_UNSIGNED_BYTE)
        screen_surf = pygame.image.fromstring(buffer, [600, 600], "RGBA")
        img2 = cv.transpose(pygame.surfarray.array3d((pygame.transform.flip(screen_surf, False, True))))
        self.obj_right = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

        # Chargement des paramètres stéréo de la caméra :
        data = np.load("calibration/data.npz")
        l_data = ["mtx1", "dist1", "mtx2", "dist2", "R", "t", "E", "F"]
        mtx1, dist1, mtx2, dist2, R, t, E, F = [data[i] for i in l_data]

        # Enlever la distorsion
        self.obj_left = cv2.undistort(self.obj_left, mtx1, dist1)#, None, mtx1)
        self.obj_right = cv2.undistort(self.obj_right, mtx2, dist2)#, None, mtx2)

        # Premier matching :
        kp1, des1 = self.sift_matcher.detectAndCompute(self.obj_left, None)
        kp2, des2 = self.sift_matcher.detectAndCompute(self.obj_right, None)
        matches = self.bf_matcher.knnMatch(des1, des2, k=2)

        if len(matches) > 1:
            # Filtrage des points :
            good = []
            for m, n in matches:
                if m.distance < 0.75 * n.distance:
                    good.append([m])

            pts1 = np.float32([kp1[m[0].queryIdx].pt for m in good])
            pts2 = np.float32([kp2[m[0].trainIdx].pt for m in good])

            # Calcul et affichage lignes épipolaires
            lines1 = cv.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
            lines1 = lines1.reshape(-1, 3)
            self.obj_left, self.obj_right = drawlines(self.obj_left, self.obj_right, lines1, pts1, pts2, kp=False)
            lines2 = cv.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
            lines2 = lines2.reshape(-1, 3)
            self.obj_left, self.obj_right = cv2.cvtColor(self.obj_left, cv2.COLOR_BGR2GRAY), cv2.cvtColor(self.obj_right, cv2.COLOR_BGR2GRAY)
            self.obj_right, self.obj_left = drawlines(self.obj_right, self.obj_left, lines2, pts2, pts1, kp=False)

            # Calul distances entre kp et lignes
            distances_left = (np.abs(lines1[:, 0] * pts1[:, 0] + lines1[:, 1] * pts1[:, 1] + lines1[:, 2])
                              / np.sqrt(lines1[:, 0] * lines1[:, 0] + lines1[:, 1] * lines1[:, 1]))
            distances_right = (np.abs(lines2[:, 0] * pts2[:, 0] + lines2[:, 1] * pts2[:, 1] + lines2[:, 2])
                               / np.sqrt(lines2[:, 0] * lines2[:, 0] + lines2[:, 1] * lines2[:, 1]))
            distances_left = distances_left < 10
            distances_right = distances_right < 10
            pts1 = pts1[distances_left]
            pts2 = pts2[distances_right]
            drawpoints(self.obj_left, self.obj_right, pts1, pts2)
            img3 = np.hstack((self.obj_left, self.obj_right))
            cv2.imshow("matches", img3)
            cv2.waitKey(1)

        # Faite un résumé de la méthode de stéréo éparse (sparse stereo) :
        # 1. Etape de calibration : on charge les paramètres stéréo de la caméra.
        # 2. On utilise les coefficients de distorsions pour dégauchir les images.
        # 3. On calcule de la même manière que dans les exercices précédents des points d'intérêts, leurs descripteurs
        # et on les comparent d'une image à l'autre pour former des couples.
        # 4. A partir de ces couples et de la matrice fondamentale F on calcule les droites épipolaires.
        # 5. On calcule la distance des points d'un couple par rapport à sa droite correspondante et on filtre selon
        # un critère arbitraire
        # 6. On affiche les droites et les points restants.


if __name__ == "__main__":
    print("CALIBRATION CAMERA")
    mtx1, dist1 = calibrate_camera("calibration/*.png", 1)
    mtx2, dist2 = calibrate_camera("calibration/*.png", 0)
    R, t, E, F = stereo_calibrate(mtx1, dist1, mtx2, dist2, "calibration/*.png")
    np.savez("calibration/data.npz", mtx1=mtx1, dist1=dist1, mtx2=mtx2, dist2=dist2, R=R, t=t, E=E, F=F)
