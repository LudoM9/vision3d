# Author : Mirado Rajaomarosata
# Date : 22/09/2023
import time
import sys

import pygame
from pygame.locals import *
from OpenGL.GL import *

from _OBJFileLoader import OBJ
from _StereoCamera import *


# Initialiser Pygame et créer une fenêtre
pygame.init()
WIDTH, HEIGHT = 600 * 3, 600
display = (WIDTH, HEIGHT)
pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

# Chargement du fichier CAO obj
# obj = OBJ("cv_checkerboard.obj")
obj = OBJ("solids.obj")

# Couleur du fond
# glClearColor(1.0, 0.85, 0.9, 1.0)
glClearColor(1.0, 1., 1., 1.0)

# Source de lumière, ombre et profondeur
glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)
obj.generate()

# Création de l'instance de la classe Camera
stereo_camera = StereoCamera((600, 600), np.array([[2.], [0.], [0.]]))

parties = ["calibration", "géométrie épipolaire", "disparités"]
question = "calibration"

if question == parties[0]:
    print("CALIBRATION CAMERA")
    mtx1, dist1 = calibrate_camera("calibration/*.png", 1)
    mtx2, dist2 = calibrate_camera("calibration/*.png", 0)
    R, t, E, F = stereo_calibrate(mtx1, dist1, mtx2, dist2, "calibration/*.png")
    np.savez("calibration/data.npz", mtx1=mtx1, dist1=dist1, mtx2=mtx2, dist2=dist2, R=R, t=t, E=E, F=F)

else:
    # Boucle principale
    while True:
        stereo_camera.handle_events()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        stereo_camera.perspective_projection(obj)

        if question == parties[1]:
            stereo_camera.rectify_images(obj)
        elif question == parties[2]:
            stereo_camera.sparse_stereo(obj)

        # Mis à jour de l'affichage
        pygame.display.flip()
        pygame.time.wait(10)
