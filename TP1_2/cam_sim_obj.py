# Author : Mirado Rajaomarosata
# Date : 22/09/2023


import pygame
from pygame.locals import *
from OpenGL.GL import *

from _OBJFileLoader import OBJ
from _Camera import *


# Initialiser Pygame et créer une fenêtre
pygame.init()
display = (700, 700)
pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

# Chargement du fichier CAO obj
obj = OBJ("TP1_2/solids.obj")

# Couleur du fond
glClearColor(1.0, 1.0, 1.0, 1.0)

# Source de lumière, ombre et profondeur
glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)
obj.generate()


# Création de l'instance de la classe Camera
camera = Camera(display)

# Boucle principale
while True:
    camera.handle_events()

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    # Rendu de la caméra 0
    camera.perspective_projection()

    obj.render()

    # Rendu de la caméra 1
    # Compléter
    
    obj.render()

    # Mis à jour de l'affichage
    pygame.display.flip()
    pygame.time.wait(10)
