# Author : Mirado Rajaomarosata
# Date : 22/09/2023

import numpy as np
import pygame
from pygame.locals import *
from OpenGL.GL import *

class EventHandler():
    
    def handle_events(self):
        speed = 0.2
        translation_speed = speed
        rotation_speed = 0.1 * speed

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    self.mouse_down = True
                    self.old_mouse_pos = event.pos
                elif event.button == 4: # Scroll up
                    self.t += np.array([[0.], [0.], [translation_speed]])
                    pass
                elif event.button == 5: # Scroll down
                    self.t += np.array([[0.], [0.], [-translation_speed]])
                    pass
            elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                self.mouse_down = False
            elif event.type == pygame.MOUSEMOTION and self.mouse_down: # Clic gauche + mouvement souris
                dx, dy = event.pos[0] - self.old_mouse_pos[0], event.pos[1] - self.old_mouse_pos[1]
                self.old_mouse_pos = event.pos
                if dx != 0:
                    self.R = np.array([
                        [np.cos(dx * rotation_speed), 0, np.sin(dx * rotation_speed)],
                        [0, 1, 0],
                        [-np.sin(dx * rotation_speed), 0, np.cos(dx * rotation_speed)]
                    ]) @ self.R
                    pass
                if dy != 0:
                    self.R = np.array([
                        [1, 0, 0],
                        [0, np.cos(dy * rotation_speed), -np.sin(dy * rotation_speed)],
                        [0, np.sin(dy * rotation_speed), np.cos(dy * rotation_speed)]
                    ]) @ self.R
                    pass

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.t += np.array([[translation_speed], [0.], [0.]])
            pass
        if keys[pygame.K_RIGHT]:
            self.t += np.array([[-translation_speed], [0.], [0.]])
            pass
        if keys[pygame.K_UP]:
            self.t += np.array([[0.], [-translation_speed], [0.]])
            pass
        if keys[pygame.K_DOWN]:
            self.t += np.array([[0.], [translation_speed], [0.]])
            pass