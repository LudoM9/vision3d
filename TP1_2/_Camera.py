# Author : Mirado Rajaomarosata
# Date : 22/09/2023

import numpy as np
from pygame.locals import *
from OpenGL.GL import *

from _EventHandler import * 

class Camera(EventHandler):
    def __init__(self, display):
        self.display = display

        # Pose initial
        self.R = np.eye(3)
        self.t = np.array([[0.],[0.],[-20.]])

        self.mouse_down = False
        self.old_mouse_pos = (0, 0)

    def perspective_matrix(self):
        fov = 45.
        aspect = self.display[0] / float(self.display[1])

        # Paramètres intrinsèques
        fx = (aspect * 1) / np.tan(np.radians(fov) / 2)
        fy = fx
        hx = 1
        hy = hx
        cx = 0
        cy = 0

        # Matrice des paramètres intrinsèques en coordonnées homogènes
        # Modifier

        K_homogeneous = np.array([
            [fx/hx, 0, cx, 0],
            [0, fy/hy, cy, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ], dtype=np.float32)

        # Matrice des paramètres extrinsèques en coordonnées homogènes
        Rt_homogeneous = np.array([
            [self.R[0, 0], self.R[0, 1], self.R[0, 2], self.t[0, 0]],
            [self.R[1, 0], self.R[1, 1], self.R[1, 2], self.t[1, 0]],
            [self.R[2, 0], self.R[2, 1], self.R[2, 2], self.t[2, 0]],
            [0, 0, 0, 1]
        ], dtype=np.float32)

        # Matrice de projection en coordonnées homogènes
        P_homogeneous = K_homogeneous @ Rt_homogeneous

        # Calcul de P_gl à partir des paramètres extraits
        near = 0.1
        far = 100.0
        top = near * np.tan(np.radians(fov) / 2)
        bottom = -top
        right = top * aspect
        left = -right

        P_gl = np.array([
            [2 * near / (right - left), 0, (right + left) / (right - left), 0],
            [0, 2 * near / (top - bottom), (top + bottom) / (top - bottom), 0],
            [0, 0, -(far + near) / (far - near), -2 * far * near / (far - near)],
            [0, 0, -1, 0]
        ], dtype=np.float32)

        P_gl = P_gl @ P_homogeneous
        return P_gl

    def perspective_projection(self):
        glMatrixMode(GL_PROJECTION)
        glLoadMatrixf(self.perspective_matrix().T)
        glMatrixMode(GL_MODELVIEW)
