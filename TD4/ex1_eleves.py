import numpy as np
import cv2 
from matplotlib import pyplot as plt

def optic_flow( old_frame, new_frame, window_size, min_quality = 0.01):
	
	#Q2 
	#Shi-Tomasi corner detector 
	feature_list = #complete
	
	window = int(window_size/2)
	old_frame = old_frame/255
	new_frame = new_frame/255
	
	#Q3
	kernel_x = #complete
	kernel_y = #complete
	kernel_t = #complete
	
	#Q4
	fx = #complete
	fy = #complete
	ft = #complete
	
	#Q5
	u = #complete
	v = #complete
	
	for feature in feature_list: #for every corner
		j, i = feature.ravel() #coordinates (j, i) of corner
		i = int(i)
		j = int(j)
		
		#Q6
		Ix = #complete
		Iy = #complete
		It = #complete
		
		#Q7
		A = #complete
		A_T = #complete
		b = #complete
		U = #complete
		
		u[i,j] = #complete
		v[i,j] = #complete
	
	return (u,v)
		
def drawOpticFlow( frame, u_comp, v_comp):	
	
	#define color vector field
	color = (0, 255, 0)
	
	#draw on image
	for i in range(frame.shape[0]):
		for j in range(frame.shape[1]):
			
			u = u_comp[i][j]
			v = v_comp[i][j]
			
			if u and v:
				frame = cv2.arrowedLine( frame, (i,j) , (int(round(i+u)), int(round(j+v))), color, thickness=2)
				frame = cv2.circle(frame, (i, j), 1, (0, 0, 255), -1)
	
	return (frame)



#Q1
#read images

#convert to graylevel

# Obtain (u,v) from Lucas Kanade approach
u,v = optic_flow( img1, img2_, 3, 0.05)
# draw results
img_res = drawOpticFlow( img2, u, v)


#Q8
#save img_res
