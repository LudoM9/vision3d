import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import signal
from EKF import ExtendedKalmanFilter

#compute distance of each keypoint with respect to all the others
def computeDistanceKeypoints(points, d): 
	for i in range(0,len(points)-1):
		for j in range(0,len(points)-1):
			#Q8
	return (list(filter(lambda a: a != 0.0, d)))
	

#compute divergence 
def computeDivergence(d_DeltaT, d_T):
	s = 0
	for i in range(0,len(d_DeltaT)):
		#Q9
		tmp = #complete with ratio between d_Delta - d_T and d_Delta
		s = s + tmp
	n =  #complete with n*DeltaT
	return (s/n)	

#approx. model acceleration drone 
def model(t):
    f, A = 1/80, 0.25
    omega = 2*np.pi*f
    omega_pow2 = omega**2
    return (- A*omega_pow2*np.sin(omega*t-np.pi/4))
    
#approx. model trajectory drone     
def f(t):
    f, A = 1/80, 0.25
    omega = 2*np.pi*f
    omega_pow2 = omega**2
    return (A*np.sin(omega*t-np.pi/4)+1)


#open first frame
count = 1
filename = "frames_drone/frame_00"+str(count)+".png"
oldFrame = cv2.imread(filename)

#Q1
window_Gaussian = (35,35)
oldFrame = #complete

#graylevel
oldFrame = cv2.cvtColor(oldFrame , cv2.COLOR_BGR2GRAY)


#define parameters Lucas-Kanade
lk_params = dict( winSize = (25, 25),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
                              10, 0.03)) 		   

#Q2
orb = #complete
kp_old, des1 = #complete
points = cv2.KeyPoint_convert(kp_old) 
# points? 

#init
DeltaT= 1/24 # GoPro lower/default frequency
div = []
div_filtered = []
distance = []
u = []
h = []

# Create KalmanFilter object KF
EKF = ExtendedKalmanFilter(DeltaT, 0.0,  0.1,0.1)

while True:
    
    #Q3: filename? 
    count += 1
    filename = "frames_drone/frame_00"+str(count)+".png"
    
    # read next frame
    frame = cv2.imread(filename)
    
    #stop if no more frames    
    if frame is None:
    	break
    
    #Q4
    frame = #complete  
    
    #graylevel
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY ) 
    
    #Q6: len(points) > 0?  
    if len(points) > 0:
    	# init d_DeltaT and d_T based on number of keypoints detected
    	d_DeltaT = np.zeros_like(points[:,1])
    	d_T = np.zeros_like(points[:,1])
    
    	#Q7: optic flow? 
    	points_foll, st, err = cv2.calcOpticalFlowPyrLK(oldFrame, frame, points, None, **lk_params)
    
    	# compute d(t+Delta)
    	d_DeltaT = computeDistanceKeypoints(points, d_DeltaT)
    	# compute d(t)
    	d_T = computeDistanceKeypoints(points_foll, d_T)
    	
    	#compute divergence
    	div_T= computeDivergence(d_DeltaT, d_T)
        
        # append divergence 
    	div.append(div_T) # already in rad/s
    	div_filtered.append(div_T) # already in rad/s
    else:
    	# append divergence
    	div.append(0)
    	div_filtered.append(0)
    
    #Q10: outliers? 
    if (len(div_filtered) > 2) & (abs(div_filtered[count-2] - div_filtered[count-3]) > 1):
    		div_filtered[count-2] = div_filtered[count-3]	
    		
    # EKF
    h.append(f(count-2))
    u.append(model(count-2))
    distance.append(EKF.EKF(u[count-2],div_filtered[count-2]))
    	
    #Q11
    kp_old, des2 = #complete
    points = #complete
    oldFrame = #complete
    # update? 


x = np.arange(0,len(div))
plt.figure(1)
plt.axis([0, 145, -5, 5])
plt.plot(x, div, '--b')
plt.plot(x, div_filtered, 'b')
plt.xlabel('time [s]')
plt.ylabel('divergence [rad/s]')
plt.figure(2)
plt.axis([0, 145, 0, 2])
plt.plot(x, distance,'g')
plt.plot(x, h,'--g')
plt.xlabel('time [s]')
plt.ylabel('h, h_EKF [m]')
plt.show()
