import numpy as np
from scipy.linalg import expm, sinm, cosm

class ExtendedKalmanFilter(object):
    def __init__(self, dt, u, x_std_meas, y_std_meas):

        # Define sampling time
        self.dt = dt

        # Intial State
        self.x = np.array([[1], [0.1]])

        # Define the State Transition array A
        self.A = np.array([[0,1], [0,0]])
        self.PHIk = expm(self.A * dt); 
        
        # Define the Control Input array B
        self.B = np.array([[0], [1]]) 
        self.GAMMAk = (np.linalg.pinv(self.A)@ self.PHIk - np.linalg.pinv(self.A))@ self.B;
        
        #Initial Process Noise Covariance
        self.Q = 7e-3
    
        #Initial Measurement Noise Covariance
        self.R = 1e-4

        #Initial Covariance array
        self.P = np.array([[0.05,0], [0,0.05]])


    def EKF(self, u, y):
    	self.x = self.PHIk @ self.x + self.GAMMAk * u
    	self.P = self.PHIk @ self.P @ self.PHIk.T + self.GAMMAk * self.Q * self.GAMMAk.T
    	
    	H = np.array([ -self.x[1]/(self.x[0]**2),  1/self.x[0]]).T
    	S = H @ self.P @ H.T + self.R
    	K = (self.P @ H.T) / S[0][0]
    	ba = K @ (y - (self.x[1]/self.x[0]))
    	self.x = self.x + np.array([[ba[0]] , [ba[1]]])
    	self.P = (np.eye(len(self.x)) - K @ H) @ self.P @ (np.eye(len(self.x)) - (K @ H).T) + K * self.R * K.T
 
    	return self.x[0]


