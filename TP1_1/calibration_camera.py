"""
TE1 - Calibration de caméra
Module Vision
UE5.4 ROB - Année 2022-2023
"""

import cv2 as cv
import numpy as np
import glob

print("OpenCV version:", cv.__version__)

objp = np.zeros((54,3), np.float32)
objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('TP1_1/Jeu1/frame_*.png') # https://moodle.ensta-bretagne.fr/mod/resource/view.php?id=51028
print(images)

for image in images:
    img = cv.imread(image)
    # cv.imshow('img',img)
    # cv.waitKey(500)
    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    ret, corners = cv.findChessboardCorners(gray, (9,6), None)
    if ret is True:
        objpoints.append(objp)
        imgpoints.append(corners)
        cv.drawChessboardCorners(img, (9,6), corners, ret)
        # cv.imshow('img',img)
        # cv.waitKey(500)

print("Longueur imgpoints : ", len(imgpoints))
# La longueur de imgpoints correspond au nombre d'images qui ont été traitées.
# Ici, 10 images ont été traitées.

print("Longueur imgpoints[0] : ", len(imgpoints[0]))
# La longueur de imgpoints[0] correspond au nombre de points détectés sur la première image.
# Ici, 54 points ont été détectés sur la première image.

print("Calibration de la caméra...")
ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

print("Matrice de calibration : ")
print(mtx)
print("Distorsion : ")
print(dist)
print("Vecteurs de rotation : ")
print(rvecs)
print("Vecteurs de translation : ")
print(tvecs)

# Les différentes étapes de l'algorithme de calibration sont les suivantes :
# 1. Compute the initial intrinsic parameters (the option only available for planar
#       calibration patterns) or read them from the input parameters. The distortion
#       coefficients are all set to zeros initially unless some of CALIB_FIX_K? are specified.
# 2. Estimate the initial camera pose as if the intrinsic parameters have been already known.
#       This is done using solvePnP.
# 3. Run the global Levenberg-Marquardt optimization algorithm to minimize the reprojection error,
#       that is, the total sum of squared distances between the observed feature points imagePoints
#       and the projected (using the current estimates for camera parameters and the poses) object
#       points objectPoints. See projectPoints for details.


# - La matrice de calibration est une matrice 3x3 qui contient les paramètres intrinsèques
#       de la caméra.
#   Elle est définie par :
#       - f_x : la distance focale en x
#       - f_y : la distance focale en y
#       - c_x : la coordonnée du centre optique en x
#       - c_y : la coordonnée du centre optique en y

# - La distorsion est un vecteur de 5 coefficients qui contient les paramètres extrinsèques
#       de la caméra.
# - La fonction ne retourne que 5 paramètres sur les 12 calculables car les 7 autres sont
#       des paramètres extrinsèques qui dépendent de la position de la caméra dans l'espace.
# - Ces paramètres dépendent de la scène observée et de la position de la caméra par rapport
#       à cette scène.

# - Les vecteurs de rotation et de translation sont des vecteurs 3x1 qui contiennent les
#       paramètres extrinsèques de la caméra.
# - L'unité associée à ces paramètres est le mètre.

def draw(img, corners, imgpts):
    corner = tuple(corners[0].astype(int).ravel())
    img = cv.line(img, corner, tuple(imgpts[0].astype(int).ravel()), (255,0,0), 5)
    img = cv.line(img, corner, tuple(imgpts[1].astype(int).ravel()), (0,255,0), 5)
    img = cv.line(img, corner, tuple(imgpts[2].astype(int).ravel()), (0,0,255), 5)
    return img

axis = np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
i = 0

for image in images:
    img = cv.imread(image)
    # cv.imshow('img',img)
    # cv.waitKey(500)
    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    ret, corners = cv.findChessboardCorners(gray, (9,6), None)
    if ret is True:
        corners2 = cv.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        # project 3D points to image plane
        imgpts, jac = cv.projectPoints(axis, rvecs[i], tvecs[i], mtx, dist)
        img = draw(img,corners2,imgpts)
        # cv.imshow('img',img)
        # k = cv.waitKey(1000)
    i += 1

# La qualité de la calibration est bonne car les points projetés sur les images
# correspondent bien aux points réels.

# Calcul de l'erreur de re-projection:
mean_error = 0
for i, objpoint in enumerate(objpoints):
    imgpoints2, _ = cv.projectPoints(objpoint, rvecs[i], tvecs[i], mtx, dist)
    error = cv.norm(imgpoints[i],imgpoints2, cv.NORM_L2)/len(imgpoints2)
    mean_error += error

print("Erreur de re-projection moyenne : ", mean_error/len(objpoints))

# Sauvegarde des paramètres de calibration dans un fichier
np.savez('calibration.npz', mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)

# Correction des distorsions
# - La fonction qui permet de corriger les distorsions est la fonction undistort.
# - Elle prend en paramètres l'image à corriger, la matrice de calibration et le
#       vecteur de distorsion.
# - Elle calcule la matrice de transformation qui permet de corriger les distorsions
#       et l'applique à l'image.
