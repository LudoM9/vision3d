# Author : Mirado Rajaomarosata
# Date : 22/09/2023

import numpy as np
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import cv2

from detector import *
from KalmanFilter import KalmanFilter

def circle2ellipse(x,y,rap):
        # rap : rapport d'agrandissement de l'axe principal par rapport à l'axe secondaire
        return x * rap, y

def rotate_ellipse(x,y,rot):
        # rot est en radian
        return x * np.cos(rot) - y * np.sin(rot), x * np.sin(rot) + y * np.cos(rot)

def draw_sattelite(t, deph, radius, rap, rot, color, use_depth_test=True):
    if use_depth_test:
        glEnable(GL_DEPTH_TEST)
    else:
        glDisable(GL_DEPTH_TEST)

    # Trajectoire circulaire de base
    x = radius * np.cos(np.radians(t + deph))
    y = radius * np.sin(np.radians(t + deph))

    # Déformation en trajectoire elliptique 
    x, y = circle2ellipse(x,y,rap) # Complétez la fonction

    # Rotation de l'ellipse 
    x, y = rotate_ellipse(x,y,rot)

    glColor3f(*color)
    glPushMatrix()
    glTranslatef(x, y, 0)
    gluDisk(gluNewQuadric(), 0, 1, 100, 1)
    glPopMatrix()

def accel(t, deph, radius, rap, rot):
    alpha = 42.5
    ux = -(np.pi/180)**2*np.cos(np.pi*(deph/180 + t/180)) * alpha * radius
    uy = -(np.pi/180)**2*np.sin(np.pi*(deph/180 + t/180)) * alpha * radius

    ux, uy = circle2ellipse(ux,uy,rap)
    ux, uy = rotate_ellipse(ux,uy,rot)

    return np.array([[ux],[-uy]])

# Initialiser Pygame et créer une fenêtre
pygame.init()
display = (700, 700)
pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

glClearColor(1.0, 1.0, 1.0, 1.0)

glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)

glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)

# Paramètres de l'animation
glTranslatef(0.0, 0.0, -20)
t = 0
radius = 2.5
deph_glob = np.pi/6

# Create KalmanFilter object KF
KF = KalmanFilter(1, 0.0, 0.0, 1, 0.1,0.1)

# Boucle principale
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    # Dessiner l'astre
    glEnable(GL_DEPTH_TEST)
    glColor3f(0.7, 0.7, 0.7)  # Gris
    gluDisk(gluNewQuadric(), 0, 6, 100, 1)

    # Alterner l'affichage devant / derrière artificiellement
    if t % 360 < 360/2: draw_sattelite(t, 0, radius, 3, 0*2*np.pi/3+deph_glob, (1.0, 0.0, 0.0), use_depth_test=False)
    else: draw_sattelite(t, 0, radius, 3, 0*2*np.pi/3+deph_glob, (1.0, 0.0, 0.0), use_depth_test=True)
    if (t+120) % 360 < 360/2: draw_sattelite(t, 120, radius, 3, 1*2*np.pi/3+deph_glob, (0.0, 1.0, 0.0), use_depth_test=False)
    else: draw_sattelite(t, 120, radius, 3, 1*2*np.pi/3+deph_glob, (0.0, 1.0, 0.0), use_depth_test=True)
    if (t+240) % 360 < 360/2:draw_sattelite(t, 240, radius, 3, 2*2*np.pi/3+deph_glob, (0.0, 0.0, 1.0), use_depth_test=False)
    else: draw_sattelite(t, 240, radius, 3, 2*2*np.pi/3+deph_glob, (0.0, 0.0, 1.0), use_depth_test=True)
    
    (x, y) = KF.x[0:2]

    # Detect object
    detect_OK = 1
    centers, frame = None,None
    if detect_OK:
        centers, frame = blue_ball_detect(display)

    Kalman_prediction_OK = 1
    if Kalman_prediction_OK:
        # Kalman prediction
        u_scene = accel(t, 240, radius, 3, 2*2*np.pi/3+deph_glob)
        (x, y) = KF.predict(u_scene)

        cv2.rectangle(frame, (int(x - 15), int(y - 15)), (int(x + 15), int(y + 15)), (0, 0, 255), 2)
        cv2.putText(frame, "Predicted Position", (int(x + 15), int(y)), 0, 0.5, (0, 0, 255), 2)
    
    if detect_OK:
        # Kalman update
        if (len(centers) > 0):
            # Draw the detected circle
            cv2.circle(frame, (int(centers[0][0]), int(centers[0][1])), 10, (0, 191, 255), 2)
            cv2.putText(frame, "Measured Position", (int(centers[0][0] + 15), int(centers[0][1] - 15)), 0, 0.5, (0,191,255), 2)

            Kalman_update_OK = 1
            if Kalman_update_OK:
                (x1, y1) = KF.update(centers[0])
                # Draw a rectangle as the estimated object position
                cv2.rectangle(frame, (int(x1 - 15), int(y1 - 15)), (int(x1 + 15), int(y1 + 15)), (0, 255, 0), 2)
                cv2.putText(frame, "Updated Position", (int(x1 + 15), int(y1 + 10)), 0, 0.5, (0, 255, 0), 2)
        
        # Afficher l'image avec la détection dans une fenêtre OpenCV séparée
        cv2.imshow('Blue Sattelite Detection', frame)
        cv2.waitKey(1)

    pygame.display.flip()
    pygame.time.wait(10)
    t += 1

