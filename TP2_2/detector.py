# Author : Mirado Rajaomarosata
# Date : 22/09/2023

import numpy as np
from OpenGL.GL import *
from OpenGL.GLU import *
import cv2

def blue_ball_detect(display):
    glReadBuffer(GL_FRONT)
    pixels = glReadPixels(0, 0, display[0], display[1], GL_RGB, GL_UNSIGNED_BYTE)
    image = np.frombuffer(pixels, dtype=np.uint8).reshape(display[1], display[0], 3)[::-1, :]

    # Déterminez dans image le centre (center_x, center_y) du sattelite bleu avec la méthode
    #       de votre choix
    # Detect blue ball using OpenCV

    # Convertir l'image OpenGL en une image OpenCV
    image_opencv = np.zeros_like(image)
    image_opencv[:,:,0] = image[:,:,2]
    image_opencv[:,:,1] = image[:,:,1]
    image_opencv[:,:,2] = image[:,:,0]

    # define range of blue color in BGR
    lower_blue = np.array([100,0,0])
    upper_blue = np.array([255,10,10])

    # Threshold the BGR image to get only blue colors
    mask = cv2.inRange(image_opencv, lower_blue, upper_blue)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(image_opencv,image_opencv, mask= mask)

    # Convert BGR to GRAY
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)


    # Find contours
    ret,thresh = cv2.threshold(gray,10,100,0)
    contours,hierarchy = cv2.findContours(thresh, 1, 2)

    if len(contours) == 0:
        return [], image_opencv

    M = cv2.moments(contours[0])
    if M["m00"] == 0:
        return [], image_opencv
    center_x = int(M["m10"] / M["m00"])
    center_y = int(M["m01"] / M["m00"])

    centers = []
    centers.append(np.array([[center_x],[center_y]]))

    return centers, image_opencv
